Capacitor NFC Plugin
==========================

Read and write NFC tags in your Capacitor app.

Supported Platforms
-------------------

* Android
* (TODO) iOS


## showSettings

Show the NFC settings on the device.

    NFC.showSettings();

### Supported Platforms

- Android
